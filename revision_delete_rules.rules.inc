<?php

/**
 * @file
 * Provide integration with rules for deleting a node revision.
 */

/**
 * Implements hook_rules_action_info().
 */
function revision_delete_rules_rules_action_info() {
  $actions['revision_delete_rules_delete_revision'] = array(
    'label' => t("Delete a node revision"),
    'group' => t("Node"),
    'base' => 'revision_delete_rules_action_delete_revision_action',

    'parameter' => array(
      'node' => array(
        'type' => 'node',
        'label' => t("Content"),
      ),
      'confirmation' => array(
        'type' => 'boolean',
        'label' => t("Confirmation required"),
        'restriction' => 'input',
      ),
    ),
  );
  return $actions;
}

/**
 * Action: Delete a revision.
 *
 * @param $node
 *  The node that the delete revision action takes place on.
 * @param $confirmation
 *   If set to TRUE, then the user is redirected to the revision delete confirmation screen.
 *   If set to FALSE, then the revision is deleted without requiring user confirmation.
 */
function revision_delete_rules_action_delete_revision_action($node, $confirmation) {
  $access = TRUE;
  drupal_alter('revision_delete_access', $access, $node);
  if ($access) {
    if ($confirmation) {
      // If confirmation is required, go to the delete confirm screen.
      drupal_goto('node/' . $node->nid . '/revisions/' . $node->vid . '/delete');
    }
    else {
      // If confirmation is NOT required, delete the revision.
      node_revision_delete($node->vid);
    }
  }
  else {
    drupal_set_message(t('You are not allowed to delete this revision.'), 'error');
  }
}
