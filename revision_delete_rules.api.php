<?php

/**
 * @file
 * API documentation file for Revision Delete Rules.
 */

/**
 * Allows modules to alter revision delete access.
 *
 * @param &$access
 *   A boolean access declaration. Passed by reference.
 * @param $node
 *   The node being acted upon.
 */
function hook_revision_delete_rules_access_alter(&$access, $node) {

}